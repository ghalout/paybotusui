// import { version } from '../package.json';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { ChatAppModule } from './app/app.module';

export class ChatBot {
    public readonly version: string = '1.0.46';
    private settings: ChatBotSetting;
    private callbackQueues = {};
    public get setting() {
        return { ...this.settings };
    }
    public render(settings: ChatBotSetting) {
        this.settings = settings;
        const rootElem = document.querySelector(this.setting.selector);
        const appRoot = document.createElement('app-root1');
        rootElem.append(appRoot);
        platformBrowserDynamic().bootstrapModule(ChatAppModule)
            .catch(err => console.error(err));
    }
    public register(eventName: string, callback: (param: any) => any) {
        this.callbackQueues[eventName] = callback;
    }
    public invoke(eventName: string, params: any) {
        const func = this.callbackQueues[eventName];
        if (func) {
            func(params);
        }
    }
}

export interface ChatBotSetting {
    selector: string;
    tla: string;
}
