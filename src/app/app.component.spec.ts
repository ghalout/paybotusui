import { TestBed, async, inject } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { CookieService } from 'ngx-cookie-service';
import { ChatBot } from './entities/chatBot';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Http } from '@angular/http/src/http';
import { AppService } from './app.services';
import { Ng2DeviceService } from 'ng2-device-detector';

describe('AppComponent', () => {

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, HttpClientModule, FormsModule, ReactiveFormsModule],
      declarations: [
        AppComponent
      ],
      providers: [AppService, CookieService, Ng2DeviceService]
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
  it(`should have as title 'app'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;

    expect(app.title).toEqual('app');
  }));

  it('should create an instance of chatBot', () => {
    const chat = new ChatBot();
    expect(chat).toBeTruthy();
    expect(chat.inputMessage).toBe('');

  });


  it('should create chatbot  service',
    inject([AppService], (appService: AppService) => {

      expect(appService).toBeTruthy();


    }));
  it('service fuction exists or not',
    inject([AppService], (appService: AppService) => {

      expect(appService.chatQuery).toBeTruthy();


    }));
  it('test service ',
    inject([AppService], (appService: AppService) => {
      const data = {
        'requestType': 'bubble',
        'info': {},
        'responseType': 'json',
        'input': { 'message': '', 'data': { 'parentId': 0 }, 'tag': 'General_Greetings', 'context': {} }
      };
      appService.chatQuery(JSON.stringify(data)).subscribe(
        (successResult) => {



        });

    }));

  it('should not display the modal unless the button is clicked', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;

    expect(app.isChatBotvisibility).toEqual(false);



  });
  //  it('display the chat bot', () => {
  //      const fixture = TestBed.createComponent(AppComponent);
  //      const app = fixture.debugElement.componentInstance;
  //      app.isChatBotvisibility= false
  //      app.data  = {
  //       'requestType': 'bubble',
  //       'info': {},
  //       'responseType': 'json',
  //       'input': { 'message': '', 'data': { 'parentId': 0 }, 'tag': 'General_Greetings', 'context': {} }
  //     }
  //        app.chatBotvisibility();

  //     fixture.detectChanges();
  //     fixture.whenStable().then( () => {
  //         fixture.detectChanges();

  //     });
  // });

});
