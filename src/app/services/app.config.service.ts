import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { HttpClient } from '@angular/common/http';
@Injectable()
export class AppConfig {

    static settings: any;

    constructor(private http: HttpClient) { }

    load() {
        const jsonFile = 'assets/config/config.json';
        return new Promise<void>((resolve, reject) => {
            this.http.get(jsonFile).toPromise().then(
                res => { // Success
                  AppConfig.settings = res;
                  resolve();
                }
              ).catch((response: any) => {
                reject(`Could not load file '${jsonFile}': ${JSON.stringify(response)}`);
            });
        });
    }
}
