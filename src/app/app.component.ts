/// <reference types='@types/googlemaps' />
import { Component, OnInit, ViewChild, ElementRef, NgZone } from '@angular/core';
import moment from 'moment';
import { ChatBot } from './entities/chatBot';
import { Signup, DayValue, EmailAddress, PaymentInfo, AccountInfo, Paydate, CreditCard, EcheckPay, FeedBack } from './entities/chatBot';

import { AppService } from './app.services';
import { UserIdleService } from 'angular-user-idle';
import { Path } from './shared/common/config';
import { Ng2DeviceService } from 'ng2-device-detector';
import { AppConfig } from './services/app.config.service';
import { DataModels } from './DataModels/dataModel';


// import * as angular from 'angular';
@Component({
  selector: 'app-root1',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],


})
export class AppComponent implements OnInit {
  model: Signup = new Signup();
  payInfo: PaymentInfo = new PaymentInfo();
  accountInfo: AccountInfo = new AccountInfo();
  date: Paydate = new Paydate();
  daySelect: DayValue = new DayValue();
  creditCard = new CreditCard();
  EcheckPay = new EcheckPay();
  FeedBack = new FeedBack();
  emailAdd = new EmailAddress();
  title = 'app';
  isChatBotvisibility: boolean;
  configPath: Path = new Path();
  chatArr = [];
  inputMessage: string;
  responseData: any;
  inputDisable: boolean;
  isSpinerShow: boolean;
  addChat = new ChatBot();
  dayValueSelected = '';
  brainContext = [];
  limittext: any;
  contextShow: boolean;
  width: any;
  info: {};
  data: any;
  tag: any;
  hideDate: boolean;
  maxheigh: boolean;
  countpayAvtar: any;
  closeIconImg: boolean;
  isDisable: boolean;
  currentURL = '';
  stepsArray = [];
  stepsIndex = -1;
  seqNo: any;
  dummyUserForm: boolean;
  applicationCompleteUrl = '';
  expiredDate: any;
  cookieExists: boolean;
  defaultMessage: string;
  chatHistoryArr = [];
  chatlogscokieExists: boolean;
  deviceInfo = null;
  iscrosIcon: boolean;
  isDeviceDesktop: boolean;
  nodeTypeId: any;
  isDivVisible: boolean;
  parentId: number;
  infoExist: boolean;
  chatWidgetSettings = [];
  billamount: any;
  headerText: string;
  headerTextColor: {};
  headerBgColor: string;
  agentBgColor: string;
  agentTextColor: string;
  viTextColor: string;
  viBgColor: string;
  payList = [];
  chatWidgetUrl: string;
  avatarUrl: string;
  bubbleBorder: {};
  bubbleArr = [];
  backendUrl: string;
  databaseName: string;
  roomDetails: any;
  formSubmitted = true;
  isAgentTyping: boolean;
  iscreditCard: Boolean;
  cardNumber: any;
  totalAmount: any;
  status: any;
  cdate: any;
  additionalData: {};
  referencenumber: any;
  paymentMethod: any;
  isaccount: boolean;
  isTextHide: boolean;
  ispaydate: boolean;
  isPaymentInfo: boolean;
  ispaymentMetod: boolean;
  ispaymentConfirm: boolean;
  iseheckForm: boolean;
  mindate: Date;
  isEcheckPayemnt: boolean;
  workspaceId: string;
  isfeedback: boolean;
  isDayDrop: boolean;
  isConfirmAutoPay: boolean;
  isTalkingToLiveAgent = false;
  isTlaType: string;
  isEmail: boolean;
  isaccountNumber: boolean;
  duedate: string;
  ispaymentLaterConfirm: boolean;
  isPayDateEcheckPayemnt: boolean;
  isfeedSuccess: boolean;
  // TODO: remove unsed dependency
  constructor(

    private appService: AppService,

    private deviceService: Ng2DeviceService,
    private userIdle: UserIdleService
  ) {
    this.isTlaType = '';
    this.isDayDrop = false;
    this.width = 360;
    this.isConfirmAutoPay = false;
    this.isfeedback = false;
    this.isEcheckPayemnt = false;
    this.isTextHide = true;
    this.ispaymentConfirm = false;
    this.ispaymentMetod = false;
    this.ispaydate = false;
    this.isPaymentInfo = false;
    this.iseheckForm = false;
    this.isaccount = false;
    this.isChatBotvisibility = false;
    this.isSpinerShow = false;
    this.maxheigh = false;
    this.countpayAvtar = 0;
    this.closeIconImg = true;
    this.isDisable = false;
    this.nodeTypeId = 0;
    this.iscreditCard = false;
    this.cdate = new Date();

    this.currentURL = '';
    this.isDivVisible = false;

    this.applicationCompleteUrl = '';
    this.isEmail = false;
    this.isaccountNumber = false;
    this.ispaymentLaterConfirm = false;
    this.isPayDateEcheckPayemnt = false;
    this.isfeedSuccess = false;




    if (('conversationId' in localStorage)) {

      this.additionalData = {
        'source': 'web', 'tla': window.payBotUs.setting.tla,
        'conversationId': localStorage.getItem('conversationId')
      };

    } else {
      this.additionalData = { 'source': 'web', 'tla': window.payBotUs.setting.tla };
    }
    this.chatHistoryArr = [];

    this.hideDate = false;
    this.date.date = new Date();
    this.mindate = new Date();
    this.mindate.setDate(this.mindate.getDate() + 1);
    this.chatWidgetUrl = 'https://paxcelchatbotbackup.s3.amazonaws.com/payementusBot/chat-icon.png';
    this.backendUrl = AppConfig.settings.liveAgnetChat;

  }
  ngOnInit() {


    this.appService.accountNumberEvent.subscribe((value) => {
      const accountData = {
        'convId': localStorage.getItem('conversationId'),
        'tla': window.payBotUs.setting.tla,
        'data': {
          'accountNumber': value
        }
      };
      this.appService.addToContext(accountData)
        .subscribe((res: any) => {
        },
          error => {
          });
    });
    const tla = { 'tla': window.payBotUs.setting.tla };
    this.chatSetting(tla);
    this.deviceInfo = this.deviceService.getDeviceInfo();
    // console.log(this.deviceInfo);
    this.contextShow = false;
    this.limittext = 200;
    this.chatArr = [];

    this.info = {};
    if (/mobile/i.test(navigator.userAgent)) {
      this.isDeviceDesktop = false;
    } else {
      this.isDeviceDesktop = true;

    }







  }
  // scroll left
  public explorescrollLeft(e): void {
    document.querySelector('div.exploremiddle').scrollLeft -= 310;
  }

  public explorescrollRight(e): void {
    const tt = document.querySelector('div.exploremiddle').scrollWidth - 310;
    const btt = tt - document.querySelector('div.exploremiddle').scrollLeft;
    if (btt >= 310) {
      document.querySelector('div.exploremiddle').scrollLeft += 310;
    }
  }
  stop() {
    this.userIdle.stopTimer();
  }

  stopWatching() {
    this.userIdle.stopWatching();
  }

  startWatching() {
    this.userIdle.startWatching();
  }

  restart() {
    this.userIdle.resetTimer();
  }
  addContextVariable(data) {
    this.appService.addToContext(data)
      .subscribe((res: any) => {
      },
        error => {
        });
  }
  // envent for feedback
  feedBackSubmit() {
    const data = {
      'type': 'text',
      'input': { 'text': this.FeedBack.feedBack },

      'additionalData': this.additionalData
    };
    this.isfeedSuccess = true;
    this.appService.feedback(JSON.stringify(data))
      .subscribe((res: any) => {
        setTimeout(() => {
          this.isfeedback = false;
          this.isfeedSuccess = false;
          this.FeedBack.feedBack = '';
        }, 1500);

      },
        error => {
          setTimeout(() => {
            this.isfeedback = false;
            this.isfeedSuccess = false;
            this.FeedBack.feedBack = '';
          }, 1500);

        });
  }
  // Function for Feedback popup show or hide

  FeedbackForm() {
    if (this.isfeedback) {
      this.isfeedback = false;
    } else {
      this.isfeedback = true;
    }
  }


  // function for getting widgets settings
  chatSetting(data) {
    // get api for chatbotsettings
    this.appService.getChatbotSetting(data)
      .subscribe((res: any) => {

        // console.log(res)
        this.chatWidgetSettings = res.settings;
        this.isTlaType = res.tla;
        for (let i = 0; i < this.chatWidgetSettings.length; i++) {

          const prop = this.chatWidgetSettings[i].key;
          switch (prop) {
            case 'theme_id':

              break;
            case 'avatar':
              this.avatarUrl = this.chatWidgetSettings[i].value.url;
              break;
            case 'icon_visible_id':

              break;
            case 'chatbot_widget':
              this.chatWidgetUrl = this.chatWidgetSettings[i].value.iconurl;

              break;
            case 'widget_position':

              break;
            case 'header_bg_color':
              this.headerBgColor = this.chatWidgetSettings[i].value.color;
              break;
            case 'header_text_color':

              this.headerTextColor = { 'color': this.chatWidgetSettings[i].value.color };

              break;
            case 'ag_msg_bg_color':
              this.agentBgColor = this.chatWidgetSettings[i].value.color;
              break;
            case 'ag_text_color':
              this.agentTextColor = this.chatWidgetSettings[i].value.color;

              break;
            case 'vi_msg_bg_color':
              this.viBgColor = this.chatWidgetSettings[i].value.color;

              break;
            case 'vi_text_color':
              this.viTextColor = this.chatWidgetSettings[i].value.color;
              this.bubbleBorder = { 'border': '1px solid ' + this.chatWidgetSettings[i].value.color + '!important;' };

              break;
            case 'header_text':
              this.headerText = this.chatWidgetSettings[i].value.name;
              break;
            default:

          }
        }
        if (this.isDeviceDesktop) {
          setTimeout(() => {

            if (!('isCrosClick' in localStorage)) {
              this.chatBotvisibility();
            }

          }, 2500);
        }

      },
        error => {
          console.log('error');
        });
  }

  // function for handling echeck payment
  submitEcheckInfo() {


    this.chatArr.splice(this.chatArr.length - 1, 1);
    this.data = {
      'type': 'template',
      'input': {
        'type': 'form',
        'data': {
          'formName': 'EcheckConfirmPayment',
          'fields': {
            'paytype': this.EcheckPay.paytype,
            'routingNumber': this.EcheckPay.routingnumber,
            'bankName': this.EcheckPay.bankname,
            'accountNumbercheck': this.EcheckPay.accnumber
          }
        }
      },

      'additionalData': this.additionalData
    };


    this.iseheckForm = false;



    this.ChatBot();
  }

  // function for handling echeck payment confirmation
  confirmEcheckPyment() {


    this.chatArr.splice(this.chatArr.length - 1, 1);
    this.data = {
      'type': 'template',
      'input': {
        'type': 'form',
        'data': { 'formName': 'ECheckPaymentReciept' }
      },

      'additionalData': this.additionalData
    };


    this.iseheckForm = false;



    this.ChatBot();
  }
  /*
  bubbleClicked function is called whenever a bubble click happens on the chatbot
  optionId: Id of the options like durationId,categoryId,subcategoryId,tripCodesId selected on chatbot
  optionName: Name of the options like durationId,categoryId,subcategoryId,tripCodesId selected on chatbot
  */
  onItemChange(item) {
    this.paymentMethod = item;
  }
  bubbleClicked(message) {

    const queryWithId = {
      id: this.configPath.userIdentificationId,
      message: message
    };

    this.chatArr.push(queryWithId);
    this.data = {
      'type': 'message',
      'input': {
        'type': 'text',
        'message': message
      },

      'additionalData': this.additionalData
    };

    this.stepsArray.push(this.data);
    this.bubbleArr = [];
    this.stepsIndex++;
    // let element = document.getElementById('bubbleBoxId');
    // element.parentNode.removeChild(element);
    this.ChatBot();
  }

  // function trigger when we click on cros Icon
  crosIcon() {

    // this.cookieService.set('isCrosClick', 'true');
    localStorage.setItem('isCrosClick', 'true');
  }
  // Function trigger when we click on image


  // function trigger when  we click on bot icon
  chatBotvisibility() {
    localStorage.removeItem('visitorId');
    localStorage.removeItem('isCrosClick');
    this.isDivVisible = false;


    this.width = 360;
    if (this.isChatBotvisibility === false && this.countpayAvtar === 0) {

      this.countpayAvtar++;
      this.isChatBotvisibility = true;

      this.maxheigh = true;

      // general greetings rule is fired
      if (this.cookieExists && this.chatlogscokieExists && this.infoExist) {

        setTimeout(() => {
          this.isSpinerShow = false;
          this.info = {};
          this.data = {
            'type': 'message',
            'input': {
              'type': 'text',
              'message': 'hello'
            },

            'additionalData': this.additionalData
          };
          if (this.isTlaType !== 'DEFAULT') {
            this.ChatBot();
          } else {
            const replyWithId = {
              id: this.configPath.simpleMessageId,
              message: 'Currently we are not providing chatbot service for ' + window.payBotUs.setting.tla + 'biller'
            };
            this.chatArr.push(replyWithId);

          }

          this.stepsArray.push(this.data);


          this.stepsIndex++;
        }, 2000);
      } else {
        this.startAgainData();

      }

    } else if (this.isChatBotvisibility === false && this.countpayAvtar !== 0) {

      // <<<---    using ()=> syntax
      this.isChatBotvisibility = true;
      this.maxheigh = true;
    } else {

      this.isChatBotvisibility = false;
      this.maxheigh = false;


    }

  }
  updateFeed(chat) {

    this.inputMessage = chat;
    const replyWithId = {
      id: this.configPath.simpleMessageId,
      message: chat
    };
    this.chatArr.push(replyWithId);
    this.chatArr.push(new DataModels.Livechat(chat, false, true));

  }

  // now,later,updateCalcs function handle date picker component
  now() {
    this.hideDate = false;
    this.date.date = new Date();


  }
  later() {
    this.hideDate = true;
  }
  updateCalcs(event) {

    this.date.date = event;
  }

  selectDate() {
    this.chatArr.splice(this.chatArr.length - 1, 1);
    const ds = this.date.date.toString();
    const payDate = moment(new Date(ds.substr(0, 16)));
    this.data = {
      'type': 'template',
      'input': {
        'type': 'form',
        'data': {
          'formName': 'PaymentInformation',
          'fields': {
            'paydate': payDate.format('MMDDYYYY')
          }
        }
      },
      'additionalData': this.additionalData
    };
    this.stepsArray.push(this.data);

    this.stepsIndex++;
    // let element = document.getElementById('bubbleBoxId');
    // element.parentNode.removeChild(element);
    this.ChatBot();
  }
  selectPaymentMethod() {
    this.chatArr.splice(this.chatArr.length - 1, 1);
    if (this.paymentMethod === 'Bank') {
      this.data = {
        'type': 'template',
        'input': {
          'type': 'form',
          'data': { 'formName': 'ECheckDetails', 'fields': { 'isEcheck': true } }
        },

        'additionalData': this.additionalData
      };
    } else {
      this.data = {
        'type': 'template',
        'input': {
          'type': 'form',
          'data': { 'formName': 'CardDetails', 'fields': { 'iscard': true } }
        },

        'additionalData': this.additionalData
      };
    }

    this.stepsArray.push(this.data);

    this.stepsIndex++;
    // let element = document.getElementById('bubbleBoxId');
    // element.parentNode.removeChild(element);
    this.ChatBot();

  }

  // function trigger when we enter payment persoanl info
  payementInfo() {
    this.chatArr.splice(this.chatArr.length - 1, 1);
    // let queryWithId = {
    //   id: this.configPath.userIdentificationId,
    //   message: this.accountInfo.accountNumber
    // };

    // this.chatArr.push(queryWithId);
    this.data = {
      'type': 'template',
      'input': {
        'type': 'form',
        'data': {
          'formName': 'PaymentMethod',
          'fields': {
            'firstName': this.payInfo.firstName,
            'lastName': this.payInfo.lastName,
            'email': this.payInfo.email,
            'phone': this.payInfo.phoneNumber,
            'zipCode': this.payInfo.zipCode
          }
        }
      },

      'additionalData': this.additionalData
    };

    this.stepsArray.push(this.data);

    this.stepsIndex++;
    // let element = document.getElementById('bubbleBoxId');
    // element.parentNode.removeChild(element);
    this.ChatBot();
  }
  // function trigger when we enter account number and press submit
  accountNumberStore() {
    this.chatArr.splice(this.chatArr.length - 1, 1);
    const queryWithId = {
      id: this.configPath.userIdentificationId,
      message: this.accountInfo.accountNumber
    };

    this.chatArr.push(queryWithId);
    this.data = {
      'type': 'message',
      'input': {
        'type': 'text',
        'message': 'my account number is ' + this.accountInfo.accountNumber
      },

      'additionalData': this.additionalData
    };
    const accountData = {
      'convId': localStorage.getItem('conversationId'),
      'tla': window.payBotUs.setting.tla,
      'data': {
        'sessionVariable': '0'
      }
    };
    this.addContextVariable(accountData);

    // Start watching for user inactivity.
    this.startWatching();


    // Start watching when user idle is starting.
    this.userIdle.onTimerStart().subscribe(count => console.log(count));
    const accountDataNotIdeal = {
      'convId': localStorage.getItem('conversationId'),
      'tla': window.payBotUs.setting.tla,
      'data': {
        'sessionVariable': '1'
      }
    };
    // Start watch when time is up.
    this.userIdle.onTimeout().subscribe(() => this.addContextVariable(accountDataNotIdeal)
    );
    this.stepsArray.push(this.data);

    this.stepsIndex++;
    // let element = document.getElementById('bubbleBoxId');
    // element.parentNode.removeChild(element);
    this.ChatBot();
  }

  accountNumberEntered() {
    this.chatArr.splice(this.chatArr.length - 1, 1);
    const queryWithId = {
      id: this.configPath.userIdentificationId,
      message: this.accountInfo.accountNumber
    };

    this.chatArr.push(queryWithId);
    this.data = {
      'type': 'message',
      'input': {
        'type': 'text',
        'message': 'my account number is ' + this.accountInfo.accountNumber
      },

      'additionalData': this.additionalData
    };
    this.ChatBot();
  }

  accountNumberLastStore() {
    this.chatArr.splice(this.chatArr.length - 1, 1);
    const queryWithId = {
      id: this.configPath.userIdentificationId,
      message: this.accountInfo.accountNumber
    };

    this.chatArr.push(queryWithId);
    this.data = {
      'type': 'template',
      'input': {
        'type': 'form',
        'data': { 'formName': 'AccountNumberLast', 'fields': { 'AccountNumberForLastBills': this.accountInfo.accountNumber } }
      },

      'additionalData': this.additionalData
    };

    this.stepsArray.push(this.data);

    this.stepsIndex++;
    // let element = document.getElementById('bubbleBoxId');
    // element.parentNode.removeChild(element);
    this.ChatBot();
  }

  enterRegisterEmail() {
    this.chatArr.splice(this.chatArr.length - 1, 1);
    const queryWithId = {
      id: this.configPath.userIdentificationId,
      message: this.emailAdd.emailAddress
    };

    this.chatArr.push(queryWithId);
    this.data = {
      'type': 'template',
      'input': {
        'type': 'form',
        'data': { 'formName': 'getAccountByEmail', 'fields': { 'emailAddress': this.emailAdd.emailAddress } }
      },

      'additionalData': this.additionalData
    };

    this.stepsArray.push(this.data);

    this.stepsIndex++;
    // let element = document.getElementById('bubbleBoxId');
    // element.parentNode.removeChild(element);
    this.ChatBot();
  }

  dontKnowAccountNo() {
    this.chatArr.splice(this.chatArr.length - 1, 1);
    this.data = {
      'type': 'template',
      'input': {
        'type': 'form',
        'data': { 'formName': 'dontKnowAccountNo', 'fields': {} }
      },

      'additionalData': this.additionalData
    };
    // let element = document.getElementById('bubbleBoxId');
    // element.parentNode.removeChild(element);
    this.ChatBot();
  }

  userInfo() {

    this.data = {
      'type': 'template',
      'input': {
        'type': 'form',
        'data': {
          'formName': 'ConfirmPayment',
          'fields': {
            'iscard': true,
            'cardNumber': this.creditCard.cardNumber,
            'cardHolderName': this.creditCard.cardHolderName,
            'cvv': this.creditCard.cvv,
            'exp': `${this.creditCard.mmexp}/${this.creditCard.yyexp}`
          }
        }
      },
      'additionalData': this.additionalData
    };
    this.creditCard.cardNumber = '';
    this.creditCard.cardHolderName = '';
    this.creditCard.cvv = '';
    this.creditCard.mmexp = '';
    this.creditCard.yyexp = '';
    this.ChatBot();
    this.iscreditCard = false;
  }

  confirmPyment() {
    this.chatArr.splice(this.chatArr.length - 1, 1);
    this.data = {
      'type': 'template',
      'input': {
        'type': 'form',
        'data': { 'formName': 'PaymentReciept' }
      },

      'additionalData': this.additionalData
    };

    this.stepsArray.push(this.data);

    this.stepsIndex++;
    // let element = document.getElementById('bubbleBoxId');
    // element.parentNode.removeChild(element);
    this.ChatBot();
  }
  confirmAutoPyment() {
    this.chatArr.splice(this.chatArr.length - 1, 1);
    this.data = {
      'type': 'template',
      'input': {
        'type': 'form',
        'data': { 'formName': 'autoPaymentReciept' }
      },

      'additionalData': this.additionalData
    };

    this.stepsArray.push(this.data);

    this.stepsIndex++;
    // let element = document.getElementById('bubbleBoxId');
    // element.parentNode.removeChild(element);
    this.ChatBot();
  }
  // function trigger when we press enter or submit button click
  onSubmit(addChat) {

    this.inputMessage = this.addChat.inputMessage;
    if (this.inputMessage !== '') {
      if (this.isTalkingToLiveAgent) {
        const chat = {
          'roomId': this.roomDetails.roomId,
          'userId': this.roomDetails.visitorId,
          'client': true,
          'message': this.inputMessage
        };
        this.appService.send('newMessage', JSON.stringify(chat));
        // this.chatArr.push(addChat);
        // this.chatArr.push(new DataModels.chat(addChat,true,false));
        const queryWithId = {
          id: this.configPath.userIdentificationId,
          message: this.inputMessage
        };

        this.chatArr.push(queryWithId);
        this.inputMessage = '';
      } else {
        this.data = {
          'type': 'message',
          'input': {
            'type': 'text',
            'message': this.inputMessage
          },

          'additionalData': this.additionalData
        };
        const queryWithId = {
          id: this.configPath.userIdentificationId,
          message: this.inputMessage
        };

        this.chatArr.push(queryWithId);

        this.ChatBot();
      }
      this.addChat.inputMessage = '';


    }


  }

  keyDownFunction(event: any, inputMessage: string) {
    if (inputMessage !== '') {
      if (event.keyCode === 13) {
        this.inputMessage = '';
      }
    }
  }

  eventSelection(event) {
    this.dayValueSelected = event;
  }
  selectDay() {
    if (this.dayValueSelected !== '') {
      this.data = {
        'type': 'template',
        'input': {
          'type': 'form',
          'data': {
            'formName': 'autoPaymentConfirmPayment',
            'fields': {
              'autopayday': this.dayValueSelected
            }
          }
        },

        'additionalData': this.additionalData
      };


      this.ChatBot();

    }
  }
  // set start again menu input here
  startAgainData() {
    this.data = {
      'type': 'message',
      'input': {
        'type': 'text',
        'message': 'hello'
      },

      'additionalData': this.additionalData
    };
    if (this.isTlaType !== 'DEFAULT') {
      this.ChatBot();
    } else {
      const replyWithId = {
        id: this.configPath.simpleMessageId,
        message: 'Currently we are not providing chatbot service for ' + window.payBotUs.setting.tla + ' biller'
      };
      this.chatArr.push(replyWithId);

    }
    this.stepsArray.push(this.data);


    this.stepsIndex++;
  }
  // generic chatbot functionalities start
  playAudio() {
    const audio = new Audio();
    audio.src = '//paxcelchatbotbackup.s3.amazonaws.com/rohit/notification.mp3';
    audio.load();
    audio.play();
  }
  // trigger when we click on cross icon
  closeIcon() {
    this.closeIconImg = false;
  }



  NumaricValidation(event: any) {
    const pattern = /[0-9\+\-\ ]/;

    const inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode !== 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  ChatBot() {
    this.isSpinerShow = true;
    // write
    this.ispaymentMetod = false;
    this.ispaymentConfirm = false;
    this.isaccount = false;
    this.isTextHide = true;
    this.ispaydate = false;
    this.isPaymentInfo = false;
    this.iseheckForm = false;
    this.isEcheckPayemnt = false;
    this.isDayDrop = false;
    this.isConfirmAutoPay = false;
    this.isEmail = false;
    this.isaccountNumber = false;
    this.ispaymentLaterConfirm = false;
    this.isPayDateEcheckPayemnt = false;
    this.appService.chatQuery(JSON.stringify(this.data))

      .subscribe((res: any) => {
        // in case the bot does not connected and no response is generated than if is executed
        if (res._body === 'null') {
          this.isSpinerShow = false;
          this.inputDisable = false;
          this.responseData = 'Sorry couldnt find anything .. Could you Rephrase that ? ';


          const replyWithId = {
            id: 0,
            message: this.responseData
          };
          this.chatArr.push(replyWithId);
          this.addChat.inputMessage = '';
          this.limittext = 200;
        } else {

          this.bubbleArr = [];
          this.additionalData['conversationId'] = res.conversationId;
          localStorage.setItem('conversationId', res.conversationId);
          for (let l = 0; l < res.data.length; l++) {

            if (res.data[l].type === 'text') {

              this.isSpinerShow = false;
              const replyWithId = {
                id: this.configPath.simpleMessageId,
                message: res.data[l].message
              };
              this.chatArr.push(replyWithId);


            }
            if (res.data[l].type === 'form') {
              if (res.data[l].data.name === 'accountNo') {
                this.isSpinerShow = false;

                // let replyWithId = {
                //   id: 200,

                // };
                // this.chatArr.push(replyWithId);
                if (res.data[l].data.data.accountNumber !== '$accountNumber') {
                  this.accountInfo.accountNumber = res.data[l].data.data.accountNumber;
                }

                this.isaccount = true;
                this.isTextHide = false;
              }

              if (res.data[l].data.name === 'accountNumber') {
                this.isSpinerShow = false;

                // let replyWithId = {
                //   id: 200,

                // };
                // this.chatArr.push(replyWithId);
                if (res.data[l].data.data.accountNumber !== '$accountNumber') {
                  this.accountInfo.accountNumber = res.data[l].data.data.accountNumber;
                }

                this.isaccountNumber = true;
                this.isTextHide = false;
              }
              if (res.data[l].data.name === 'accountNoLastBill') {


                const replyWithId = {
                  id: 208,

                };
                this.chatArr.push(replyWithId);
              }
              if (res.data[l].data.name === 'PaymentInformation') {
                this.isSpinerShow = false;
                // this.isSpinerShow = false;
                // let replyWithId = {
                //   id: 201,

                // };
                // this.chatArr.push(replyWithId);
                this.isPaymentInfo = true;
                this.isTextHide = false;
              }

              if (res.data[l].data.name === 'getAccountByEmail') {
                this.isSpinerShow = false;
                // this.isSpinerShow = false;
                // let replyWithId = {
                //   id: 201,

                // };
                // this.chatArr.push(replyWithId);
                this.isEmail = true;
                this.isTextHide = false;
              }


              if (res.data[l].data.name === 'AgentCall') {

                this.isSpinerShow = false;
                {
                  {
                    const objAsQuery = {
                      'conv_id': this.additionalData['conversationId'],
                      'tla': this.additionalData['tla']
                    };
                    this.appService.initSocket(this.backendUrl, objAsQuery);
                    this.appService.onEvent('DefaultMessage').subscribe((data) => {
                      this.isTalkingToLiveAgent = true;
                      this.roomDetails = JSON.parse(data);
                      const response = JSON.parse(data);
                      const replyWithId = {
                        id: this.configPath.simpleMessageId,
                        message: response.message
                      };
                      this.chatArr.push(replyWithId);
                    });
                    this.appService.onMessage().subscribe((data) => {
                      const messageJson = JSON.parse(data);
                      this.updateFeed(messageJson.message);
                    });
                  }

                  this.appService.onEvent('SwitchBackToBot').
                    subscribe(data => {
                      this.isTalkingToLiveAgent = false;
                      const replyWithId = {
                        id: this.configPath.simpleMessageId,
                        message: 'connecting to bot again'
                      };
                      this.chatArr.push(replyWithId);
                    });
                  this.appService.onEvent('connect').subscribe(() => {

                    console.log('Connected');

                  });


                  this.appService.onEvent('roomActive').subscribe((data) => {

                    if (!this.roomDetails) {
                      this.roomDetails = JSON.parse(data);


                      this.appService.send('ready', JSON.stringify(this.roomDetails));
                    }


                  });

                  this.appService.onEvent('notificationTyping').subscribe((data) => {
                    const response = JSON.parse(data);

                    if (response.isTyping === 'true') {
                      this.isAgentTyping = true;
                    }

                    if (response.isTyping === 'false') {
                      this.isAgentTyping = false;
                    }
                  });


                  this.appService.onEvent('AgentDisconnected').subscribe((data) => {
                    // this.chatArr.push(data);
                    const roomId = JSON.parse(data).roomId;

                    if (roomId === this.roomDetails.roomId) {
                      //   this.chatArr.push(new DataModels.chat('Agent has closed the chat',false,true));

                      // this.appService.send('disconnectClient',data);
                    }
                  });


                  this.width = 360;
                }

              }

              if (res.data[l].data.name === 'CardDetails') {

                this.isSpinerShow = false;


                this.isTextHide = false;
                this.iscreditCard = true;



              }
              if (res.data[l].data.name === 'ECheckDetails') {


                this.isSpinerShow = false;
                // let replyWithId = {
                //   id: 220,

                // };
                // this.chatArr.push(replyWithId);
                this.iseheckForm = true;
                this.isTextHide = false;


              }

              if (res.data[l].data.name === 'PaymentDate') {

                this.isSpinerShow = false;
                // let replyWithId = {
                //   id: 202,

                // };
                // this.chatArr.push(replyWithId);
                this.ispaydate = true;
              }
              if (res.data[l].data.name === 'autoPaymentConfirmPayment') {

                this.isSpinerShow = false;
                // let replyWithId = {
                //   id: 202,

                // };
                // this.chatArr.push(replyWithId);
                this.cardNumber = res.data[l].data.data.cardNumber[0];
                this.cardNumber = this.cardNumber.replace(this.cardNumber.substring(3, 15), '************');
                this.duedate = res.data[l].data.data.duedate;
                this.isConfirmAutoPay = true;
                this.isTextHide = false;

              }
              if (res.data[l].data.name === 'PaymentMethod') {
                this.isSpinerShow = false;

                // let replyWithId = {
                //   id: 203,

                // };
                // this.chatArr.push(replyWithId);
                this.ispaymentMetod = true;
                this.isTextHide = false;
              }
              if (res.data[l].data.name === 'EcheckConfirmPayment') {
                this.isSpinerShow = false;
                this.cardNumber = res.data[l].data.data.accountNumbercheck[0];
                this.billamount = res.data[l].data.data.totalAmount;
                // let replyWithId = {
                //   id: 203,

                // };
                // this.chatArr.push(replyWithId);
                this.isEcheckPayemnt = true;
                this.isTextHide = false;
              }
              if (res.data[l].data.name === 'PayDateEcheckConfirmPayment') {
                this.isSpinerShow = false;
                this.cardNumber = res.data[l].data.data.accountNumbercheck[0];
                this.billamount = res.data[l].data.data.totalAmount;
                // let replyWithId = {
                //   id: 203,

                // };
                // this.chatArr.push(replyWithId);
                this.isPayDateEcheckPayemnt = true;
                this.isTextHide = false;
              }

              if (res.data[l].data.name === 'PaymentReciept') {
                this.isSpinerShow = false;
                this.payList = [];
                this.payList = res.data[l].data.data.accetedList;
                for (let i = 0; i < res.data[l].data.data.errorList.length; i++) {
                  this.payList.push(res.data[l].data.data.errorList[i]);
                }


                // this.totalAmount = res.data[l].data.data.totalamount;
                // this.status = res.data[l].data.data.paystatus;
                // this.referencenumber = res.data[l].data.data.referencenumber;

                const replyWithId = {
                  id: 205,

                };
                this.chatArr.push(replyWithId);
              }
              if (res.data[l].data.name === 'autopaymentDayPicker') {
                this.isSpinerShow = false;
                this.isDayDrop = true;
                this.isTextHide = false;
              }

              if (res.data[l].data.name === 'sessionExpired') {
                setTimeout(() => {
                  localStorage.removeItem('isCrosClick');
                  localStorage.removeItem('conversationId');
                  window.location.reload();
                }, 2000);
              }

              if (res.data[l].data.name === 'BillSummary') {
                this.isSpinerShow = false;
                this.totalAmount = res.data[l].data.data.lpaymentamount;

                this.referencenumber = res.data[l].data.data.lrefnumber;
                const replyWithId = {
                  id: 215,

                };
                this.chatArr.push(replyWithId);
              }
              if (res.data[l].data.name === 'ViewBillDetails') {
                this.isSpinerShow = false;
                this.totalAmount = res.data[l].data.data.totalamount;

                this.referencenumber = res.data[l].data.data.referencenumber;
                const replyWithId = {
                  id: 206,

                };
                this.chatArr.push(replyWithId);
              }
              if (res.data[l].data.name === 'ConfirmPayment') {
                this.isSpinerShow = false;
                this.cardNumber = res.data[l].data.data.cardNumber[0];
                this.cardNumber = this.cardNumber.replace(this.cardNumber.substring(3, 15), '************');
                this.billamount = res.data[l].data.data.totalAmount;

                // let replyWithId = {
                //   id: 204,

                // };
                // this.chatArr.push(replyWithId);
                this.ispaymentConfirm = true;
                this.isTextHide = false;
              }
              if (res.data[l].data.name === 'PayLaterConfirmPayment') {
                this.isSpinerShow = false;
                this.cardNumber = res.data[l].data.data.cardNumber[0];
                this.cardNumber = this.cardNumber.replace(this.cardNumber.substring(3, 15), '************');
                this.billamount = res.data[l].data.data.totalAmount;

                // let replyWithId = {
                //   id: 204,

                // };
                // this.chatArr.push(replyWithId);
                this.ispaymentLaterConfirm = true;
                this.isTextHide = false;
              }




            }
            if (res.data[l].type === 'option') {

              this.isSpinerShow = false;
              if (res.data[l].data.text !== '') {
                const replyWithmessage = {
                  id: this.configPath.simpleMessageId,
                  message: res.data[l].data.text
                };
                this.chatArr.push(replyWithmessage);
              }

              this.bubbleArr = [];

              this.bubbleArr = res.data[l].data.options.options;

              const replyWithId = {
                id: 4,
                message: this.bubbleArr


              };
              this.chatArr.push(replyWithId);


            }
          }



        }
      },
        error => {
          setTimeout(() => {
            this.isSpinerShow = false;
            const replyWithId = {
              id: this.configPath.simpleMessageId,
              message: this.configPath.defaultMessage
            };
            this.chatArr.push(replyWithId);

          }, 2000);
        });
  }
}

