import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Path } from './shared/common/config';
import 'rxjs/add/operator/map';
import * as io from 'socket.io-client';
import { AppConfig } from './services/app.config.service';
import { EventEmitter, NgZone } from '@angular/core';

@Injectable()
export class AppService {
    socket: any;
    configPath: Path = new Path();
    private tla: string;
    private accountNumber: string;
    tlaChangeEvent: EventEmitter<any>;
    accountNumberEvent: EventEmitter<any>;
    constructor(private http: HttpClient, ngZone: NgZone) {
        this.tlaChangeEvent = new EventEmitter();
        this.accountNumberEvent = new EventEmitter();
        window.payBotUs.register('init', (settings: any) => {
            this.tla = settings.tla;
            ngZone.run(() => {
                this.tlaChangeEvent.emit(this.tla);
            });
        });
        window.payBotUs.register('accountNumber', (settings: any) => {
            this.accountNumber = settings.accountNumber;
            ngZone.run(() => {
                this.accountNumberEvent.emit(this.accountNumber);
            });
        });
    }

    public initSocket(backendUrl, obj): void {
        this.socket = io(backendUrl + '/liveChat', {
            query: {
                conv_id: obj.conv_id,
                biller: obj.tla,
                userType: 2,
                username: 'ABC Biller Customer',
                isAgent: false
            }
        });
    }

    public send(event: any, message: any): void {
        this.socket.emit(event, message);
    }

    public onMessage(): Observable<any> {
        return new Observable<any>(observer => {
            this.socket.on('inMessage', (data: any) => observer.next(data));
        });
    }

    public onEvent(event: any): Observable<any> {
        return new Observable<any>(observer => {
            this.socket.on(event, (data: any) => observer.next(data));
        });
    }


    addNewVisitor(backendUrl, data) {
        return this.http.post(backendUrl + '/addNewVisitor', data)
            .map(res => res).pipe(catchError(this.handlError));
    }
    chatQuery(data) {
        return this.http.post(AppConfig.settings.chat + '/chatbotEngine/', data)
            .map(res => res).pipe(catchError(this.handlError));
    }

    feedback(data) {
        return this.http.post(AppConfig.settings.chat + '/feedback/', data)
            .map(res => res).pipe(catchError(this.handlError));

    }
    getChatHistory(chatlogsConvId) {
        return this.http.get(AppConfig.settings.chat + '/chatbotEngine/?chatlogsConvId='
            + chatlogsConvId)
            .map(res => res).pipe(catchError(this.handlError));

    }
    getChatbotSetting(data) {
        return this.http.post(AppConfig.settings.chat + '/settings/', data)
            .map(res => res).pipe(catchError(this.handlError));

    }

    addToContext(data) {
        return this.http.post(AppConfig.settings.chat + '/addToContext/', data)
            .map(res => res).pipe(catchError(this.handlError));

    }


    // getSettings(Id) {



    //     return this.http.get(AppConfig.settings.chat + '/settings/'
    //         + Id)
    //         .map(res => res).pipe(catchError(this.handlError));

    // }

    handlError(error) {
        return throwError(error.error.message);
    }

}
