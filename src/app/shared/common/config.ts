import { Injectable } from '@angular/core';

@Injectable()
export class Path {
  path: string;
  userIdentificationId: number;
  typeSimpleMessage: string;
  simpleMessageId: number;
  typeBubble: string;
  apiData: string;
  nodeTypeIdMessage: number;
  nodeTypeIdBubble: number;
  chatBotSettingApiPath: string;
  defaultMessage: string;
  settingUrl: string;
  constructor() {


    this.userIdentificationId = 1;
    this.simpleMessageId = 0;
    this.typeBubble = 'Bubble';
    this.apiData = 'APIDATA';
    this.nodeTypeIdMessage = 1;
    this.nodeTypeIdBubble = 2;

    this.defaultMessage = 'Sorry, Something went wrong please try again.';





  }

}
