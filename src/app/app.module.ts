import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule, FormGroup } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
// import { BubbleModule } from './bubble/bubble.module';
import { AppService } from './app.services';
import { CookieService } from 'ngx-cookie-service';
// import { EmbedVideo } from 'ngx-embed-video';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UserIdleModule } from 'angular-user-idle';
import { Ng2DeviceDetectorModule } from 'ng2-device-detector';
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material';
import { HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppConfig } from './services/app.config.service';
export function initializeApp(appConfig: AppConfig) {
  return () => appConfig.load();
}

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    ReactiveFormsModule, BrowserAnimationsModule,
    BrowserModule, HttpClientModule, CommonModule, RouterModule, FormsModule, ReactiveFormsModule, HttpClientModule,

    MatDatepickerModule, MatNativeDateModule, MatSelectModule,
    Ng2DeviceDetectorModule.forRoot(),
    UserIdleModule.forRoot({idle: 600, timeout: 300, ping: 120})
  ],
  providers: [AppService, CookieService, MatDatepickerModule, AppConfig, {
    provide: APP_INITIALIZER,
    useFactory: initializeApp,
    deps: [AppConfig], multi: true
  }

  ],
  bootstrap: [AppComponent],
  entryComponents: [AppComponent],
  exports: [AppComponent]
})
export class ChatAppModule { }
