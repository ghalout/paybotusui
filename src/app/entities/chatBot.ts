export class ChatBot {
  public inputMessage = '';
}

export class Signup {
  public name = '';
  public lastName = '';
  public email = '';
  public phoneNumber = '';
  public language = '';
  public captcha = '';
  public userRequirements = '';
  public addComments = '';
}



export class PaymentInfo {
  public firstName = '';
  public lastName = '';
  public email = '';
  public phoneNumber = '';
  public zipCode = '';
}

export class AccountInfo {
  public accountNumber = '';
}
export class EmailAddress {
  public emailAddress = '';
}
// TODO: give a proper name to this class as date will conflict which system Class Date
export class Paydate {
  public date: Date = new Date();
}

export class Accommodation {
  public checkInDate = '';
  public checkOutDate = '';
  public room: Number = 1;
  public adults: Number = 0;
  public children: Number = 0;
}

export class CreditCard {
  public cardHolderName = '';
  public cvv = '';
  public exp = '';
  public mmexp = '';
  public yyexp = '';
  public cardNumber = '';
}
export class AnyThingElseForm {
  public anyquery = '';
}

export class FeedBack {
  public feedBack = '';
}

export class EcheckPay {
  public paytype = '';
  public routingnumber = '';
  public accnumber = '';
  public bankname = '';
  public accholdername = '';
}
export class DayValue {
  public dayVal = '';

}
