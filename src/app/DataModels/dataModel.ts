import { EventEmitter, Injectable } from '@angular/core';

export namespace DataModels {

    export class Livechat {
        message: string;
        client = false;
        agent = false;

        constructor(message, client, agent) {
            this.message = message;
            this.client = client;
            this.agent = agent;
        }
    }
}
