import { enableProdMode } from '@angular/core';
import { environment } from './environments/environment';
import { ChatBot } from './chatbot';
import { ChatAppModule } from './app/app.module';
if (environment.production) {
  enableProdMode();
}


declare global {
  interface Window { payBotUs: ChatBot; }
}
window.payBotUs = window.payBotUs || new ChatBot();

if (window.payBotUs && !environment.production) {
  window.payBotUs.render({
    selector: '#bot',
    tla: 'abc'
  });
}
